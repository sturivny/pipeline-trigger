"""Trigger for Brew/Koji builds using UMB.

See https://pagure.io/fedora-ci/messages for details.
"""
import copy
import json
import os
import re

from cki_lib import brew
from cki_lib import cki_pipeline
from cki_lib import config_tree
from cki_lib import logger
from cki_lib import messagequeue
from cki_lib import metrics
from cki_lib import misc
import prometheus_client as prometheus

LOGGER = logger.get_logger(__name__)

METRIC_PIPELINE_TRIGGERED = prometheus.Counter(
    'pipeline_triggered', 'Number of pipelines triggered')


def trigger_pipeline(gitlab_instance, trigger_to_use):
    """Process GitLab trigger requests."""
    LOGGER.debug("Triggering pipeline: gitlab_instance=%s trigger_to_use=%s",
                 gitlab_instance, trigger_to_use)
    if not misc.is_production():
        LOGGER.info("Staging environment, not triggering pipeline.")
        return
    try:
        cki_pipeline.trigger_multiple(gitlab_instance, [trigger_to_use])
    except Exception:  # pylint: disable=broad-except
        LOGGER.exception('Unable to trigger pipeline for %s',
                         trigger_to_use)
        raise

    METRIC_PIPELINE_TRIGGERED.inc()


def sanity_check(properties):
    """Verify the message is what we want.

    Args:
        properties: A dictionary of message properties.

    Returns:
        False if the message is "bad", True if we should continue processing.
    """
    if properties.get('attribute') != 'state':
        return False

    # We are not interested in messages that don't mark a completed task. Also,
    # official koji builds like to use numbers.
    if properties.get('new') not in ['CLOSED', 1]:
        return False

    # We are only interested in completed builds. koji official messgaes are
    # special again.
    if properties.get('method') != 'build' and 'build_id' not in properties:
        return False

    return True


def process_copr(triggers, message):
    """Process a message from COPR."""
    # Exclude empty automated nightly builds
    if not message.get('pkg') or not message.get('version'):
        return None

    nvr = f'{message["pkg"]}-{message["version"]}'
    if message.get('status') != 1:
        LOGGER.debug('COPR build for %s not successful', nvr)
        return None

    LOGGER.info('COPR build for %s found', nvr)
    copr = f'{message["owner"]}/{message["copr"]}'

    triggers_to_use = []
    architecture = message['chroot'].split('-')[-1]
    for trigger in triggers:
        regexp = fr'^{trigger["source_package_name"]}-\d+[.-](\S+[.-])+{trigger["rpm_release"]}'
        if re.search(regexp, nvr):
            if copr in trigger.get('.coprs', []):
                matched_trigger = trigger.copy()
                matched_trigger['owner'] = message['user']
                matched_trigger['kernel_version'] = message["version"]
                matched_trigger['copr_build'] = str(message['build'])
                matched_trigger['title'] = (f'COPR: {nvr}: {architecture} ' +
                                            f'Kernel Variant "{matched_trigger["package_name"]}"')
                matched_trigger['architectures'] = architecture
                matched_trigger['repo_name'] = copr
                matched_trigger['discovery_time'] = misc.utc_now_iso()
                if report_rules := trigger.get('.report_rules'):
                    matched_trigger['report_rules'] = json.dumps(report_rules)

                matched_trigger = config_tree.clean_config(matched_trigger)
                triggers_to_use.append(matched_trigger)

    if not triggers_to_use:
        LOGGER.info('COPR: Pipeline for %s not configured', nvr)
        return None

    return triggers_to_use


def process_message(brew_config, server_section,
                    message):
    """Process a message from Koji or Brew."""
    if 'task_id' in message:
        task_id = message['task_id']  # Koji official builds
    else:
        task_id = message['id']  # Koji scratch builds and Brew
    if not task_id:   # Some koji builds don't have related tasks
        return None
    LOGGER.info('%s: A build completed!', task_id)

    if not message.get('request'):
        LOGGER.info('%s: Task doesn\'t have a build request, ignoring',
                    task_id)
        return None

    triggers_to_use = brew.get_brew_trigger_variables(
        task_id, brew_config, server_section)

    return triggers_to_use


def get_triggers_from_config(brew_config):
    """Transform config file to individual triggers."""
    triggers = []
    for key, value in \
            config_tree.process_config_tree(brew_config).items():
        trigger = copy.deepcopy(value)
        if not trigger.get('name'):
            trigger['name'] = key
        triggers.append(trigger)
    return triggers


class AMQPMessageReceiver:
    """Receive AMQP messages and process them to generate triggers."""

    def __init__(self, triggers, brew_config, gitlab_instance):
        """Initialize message receiver."""
        self.triggers = triggers
        self.brew_config = brew_config
        self.gitlab_instance = gitlab_instance

        # Callback metrics values
        self._callback_enter_ts = None
        self._callback_exit_ts = None

    def process_umb(self, message, headers):
        """Process received message from UMB messaging bus."""
        properties = headers['message-amqp10-properties']

        if 'info' in message:
            message = message['info']

        if sanity_check(properties):
            return process_message(self.brew_config, '.amqp', message)
        return None

    def process_fedmsg(self, message):
        """Process received message from Fedora messaging bus."""
        if 'copr' in message:
            return process_copr(self.triggers, message)
        if sanity_check(message):
            if 'info' in message:  # Only scratch builds
                message = message['info']
            return process_message(self.brew_config, '.amqp091', message)
        return None

    def callback(self, body, headers, **_):
        """Process received messages."""
        bridge_name = headers['message-amqp-bridge-name']

        if bridge_name == 'fedmsg':
            triggers_to_use = self.process_fedmsg(body)
        elif bridge_name == 'umb':
            triggers_to_use = self.process_umb(body, headers)
        else:
            raise Exception(f"Can't process {bridge_name}")

        if triggers_to_use:
            for trigger in triggers_to_use:
                trigger_pipeline(self.gitlab_instance, trigger)


def poll_triggers(gitlab_instance, brew_config):
    """Create the actual triggers and start the receiver."""
    metrics.prometheus_init()

    triggers = get_triggers_from_config(brew_config)
    receiver = AMQPMessageReceiver(triggers, brew_config, gitlab_instance)

    exchange = os.environ['RABBITMQ_EXCHANGE']
    queue = os.environ['RABBITMQ_QUEUE']
    routing_keys = os.environ['RABBITMQ_ROUTING_KEYS'].split()

    connection = messagequeue.MessageQueue()
    connection.consume_messages(
        exchange, routing_keys, receiver.callback,
        queue_name=queue
    )
