"""Generic functions and constants."""
from dataclasses import dataclass
from dataclasses import field
import email
import re
import subprocess

from cki_lib import cki_pipeline
from cki_lib import gitlab
from cki_lib.logger import get_logger

LOGGER = get_logger(__name__)

# Patch name skip patterns. These patches live in kernel Patchworks but don't
# apply to the tree. Sometimes, a feature requires a change both in kernel and
# a tool closely associated with it. These are sent as a single series and
# maintainers know how to deal with them, but we don't want to download them
# because we'd just get patch application errors since the tools are not part
# of the kernel tree. We also aren't interested in pull requests, which also
# can end up in Patchwork.
PATCH_SKIP_PATTERNS = [r'\[[^\]]*iproute.*?\]',
                       r'\[[^\]]*pktgen.*?\]',
                       r'\[[^\]]*ethtool.*?\]',
                       r'\[[^\]]*git.*?\]',
                       r'\[[^\]]*pull.*?\]',
                       r'pull.?request']
PATCH_SKIP = re.compile('|'.join(PATCH_SKIP_PATTERNS), re.IGNORECASE)


@dataclass
class SeriesData:
    # pylint: disable=too-many-instance-attributes
    """Class for handling patch series data."""

    patches: list = field(default_factory=list)
    emails: set = field(default_factory=set)
    subject: str = ''
    message_id: str = ''
    last_tested: str = ''
    series_id: str = ''
    cover: str = ''
    submitter: str = ''

    def __repr__(self):
        """Return readable representation of the series."""
        return (
            f'Data for series: {self.series_id}\n'
            f'Message ID: {self.message_id}\n'
            f'Subject: {self.subject}\n'
            f'Cover letter: {self.cover}\n'
            f'Patches: {self.patches}\n'
            f'Last tested: {self.last_tested}\n'
            f'Retrieved emails: {self.emails}\n'
            f'Submitter: {self.submitter}'
        )


def get_commit_hash(repository, git_ref):
    """Return commit hash for the ref in question.

    The reference needs to include the specifier, e.g. use 'refs/heads/main'
    instead of just 'main'; to avoid collisions.

    Args:
        repository: Git repository URL.
        git_ref:    Git reference to get commit hash for.

    Returns:
        String representing the commit hash or None if an error occurred.
    """
    LOGGER.debug('Getting the last commit from %s@%s', git_ref, repository)
    try:
        lines = subprocess.check_output(
            ['git', 'ls-remote', repository],
            timeout=60
        ).decode('utf-8').split('\n')
    # pylint: disable=broad-except
    except Exception:
        LOGGER.exception('Unable to list remote %s', repository)
        return None
    matches = [line for line in lines if line.endswith(git_ref)]
    if not matches:
        LOGGER.warning('Ref %s not found in remote %s', git_ref, repository)
        return None
    return matches[0].split()[0]


def was_tested(project, cki_pipeline_branch, git_url, branch, commit_hash):
    """Find out if a commit was already tested or not.

    Args:
        project: GitLab project which the pipelines are associated with.
        cki_pipeline_branch: Project's branch responsible for testing of the repo.
        git_url: watched git repo URL
        branch: watched git repo branch
        commit_hash: current git commit hash we want to test

    Returns:
        True if the watched commit was already tested, False otherwise.
    """
    gl_pipeline = cki_pipeline.last_pipeline_for_branch(project, cki_pipeline_branch,
                                                        variable_filter={
                                                            'watch_url': re.escape(git_url),
                                                            'watch_branch': re.escape(branch),
                                                        })
    if not gl_pipeline:
        return False
    if gitlab.get_variables(gl_pipeline).get('watched_repo_commit_hash') != commit_hash:
        return False
    if len(gl_pipeline.jobs.list(per_page=1, all=False)) == 0:
        return False
    return True


def get_emails_from_headers(headers):
    """Retrieve From, To and Cc header values for given patch.

    Args:
        headers: A list of patch's email headers, grabbed from Patchwork's API
                 response. patch['headers']

    Returns:
        A set of sanitized and decoded unique email addresses.
    """
    email_headers = ['From', 'To', 'Cc']
    emails = set()

    for key in email_headers:
        if key in headers:
            unfolded = re.sub(r'\r?\n[ \t]', ' ', headers[key])
            email_data = email.utils.getaddresses([unfolded])
            for _, address in email_data:
                emails.add(address)

    return emails
