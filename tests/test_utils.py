"""Tests for triggers.utils."""
import unittest
from unittest import mock

from tests import fakes
import triggers.utils as utils


class TestSeriesData(unittest.TestCase):
    """Test cases for utils.SeriesData."""

    def test_repr_format(self):
        """Verify the string version of the objects contains everything."""
        data = utils.SeriesData(
            patches=['patch'],
            emails=set([]),
            subject='patch subject',
            series_id='123',
            message_id='<message@id>',
            last_tested='yesterday',
            submitter="joeuser@redhat.com"
        )
        attributes = ['Message ID', 'Subject', 'Cover letter', 'Patches',
                      'Last tested', 'Retrieved emails']

        data_text = repr(data)
        for attribute in attributes:
            self.assertIn(attribute, data_text)


class TestCheckForTested(unittest.TestCase):
    """Tests for utils.was_tested()."""

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_was_tested_true(self, mock_variables):
        """Check was_tested returns True if the commit was already tested."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_2', 'token', {'ref': 'cki_2'})

        mock_variables.return_value = {'watch_url': 'git_url',
                                       'ref': 'sha',
                                       'watched_repo_commit_hash': 'sha',
                                       'watch_branch': 'git_branch'}

        self.assertTrue(utils.was_tested(project, 'cki_2', 'git_url', 'git_branch', 'sha'))
        self.assertEqual(mock_variables.call_count, 2)  # found

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_was_tested_other_commit(self, mock_variables):
        """Check was_tested returns False if the pipeline didn't run."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})

        mock_variables.return_value = {'watch_url': 'git_url',
                                       'ref': 'sha',
                                       'watch_url_commit_hash': 'different-sha',
                                       'watch_branch': 'git_branch'}

        self.assertFalse(utils.was_tested(project, 'cki_branch', 'git_url', 'git_branch', 'sha'))
        self.assertEqual(mock_variables.call_count, 2)  # short-circuited

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_was_tested_no_commit(self, mock_variables):
        """Check was_tested returns False if the pipeline didn't run."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})

        mock_variables.return_value = {'watch_url': 'git_url',
                                       'ref': 'sha',
                                       'watched_repo_commit_hash': 'sha',
                                       'watch_branch': 'different_git_branch'}

        self.assertFalse(utils.was_tested(project, 'cki_branch', 'git_url', 'git_branch', 'sha'))
        self.assertEqual(mock_variables.call_count, 3)

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_was_tested_retriggered(self, mock_variables):
        """Check that was_tested skips retriggered pipelines."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})

        mock_variables.return_value = {'CKI_DEPLOYMENT_ENVIRONMENT': 'retrigger',
                                       'watch_url': 'git_url',
                                       'ref': 'sha',
                                       'watched_repo_commit_hash': 'sha',
                                       'watch_branch': 'git_branch'}

        self.assertFalse(utils.was_tested(project, 'cki_branch', 'git_url', 'git_branch', 'sha'))
        self.assertEqual(mock_variables.call_count, 3)

    @mock.patch('cki_lib.gitlab.get_variables')
    def test_was_tested_no_jobs(self, mock_variables):
        """Check was_tested returns True if the commit was already tested."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_2', 'token', {'ref': 'cki_2'})
        project.pipelines[0].jobs.clear()

        mock_variables.return_value = {'watch_url': 'git_url',
                                       'ref': 'sha',
                                       'watched_repo_commit_hash': 'sha',
                                       'watch_branch': 'git_branch'}

        self.assertFalse(utils.was_tested(project, 'cki_2', 'git_url', 'git_branch', 'sha'))


class TestGetCommitHash(unittest.TestCase):
    """Tests for utils.get_commit_hash()."""

    @mock.patch('subprocess.check_output')
    def test_commit_hash(self, mock_check_output):
        """Verify get_commit_hash returns the right value from git output."""
        mock_check_output.return_value = 'hash\t\trefs/heads/main'.encode()
        self.assertEqual(utils.get_commit_hash('repo', 'refs/heads/main'),
                         'hash')


class TestSanitizeEmails(unittest.TestCase):
    """Tests for patch_trigger.sanitize_emails()."""

    def test_missing(self):
        """Verify sanitize_emails works even if there are no such headers."""
        self.assertEqual(set(), utils.get_emails_from_headers({}))

    def test_with_data(self):
        """Verify correct headers for email retrieval are picked."""
        headers = {'HeaderX': 'invalid', 'From': 'Name Surname <name@email>',
                   'To': 'Alias <name@email>, someoneelse@email'}
        self.assertEqual(set(['name@email', 'someoneelse@email']),
                         utils.get_emails_from_headers(headers))
